$(document).ready(function() {
    // Create a client instance
    console.log("Conectando10");
    var isSubscrito=false;

    $.get("http://localhost:5000/unsubscribeAll", function(data, status){
        if(data = "OK") {
            console.log("Unsubcribe ALL.");
        }
    });

    $('#test').click(function(){
        $.get("http://localhost:5000/user/testing2", function(data, status){
            alert("Data: " + data + "\nStatus: " + status);

        });
    });

setInterval(
     function(){
        $.get("http://localhost:5000/check", function(data, status){
            console.log("Interval Data:\t"+data);

            if(data != "") {
                //$('#subscribe_messages').val(data+"\n"+$('#subscribe_messages').val);
                document.getElementById("subscribe_messages").value = data+"\n"+document.getElementById("subscribe_messages").value;
            }

        });

     },
     500  /* 10000 ms = 10 sec */
);



    //var socket = io.connect(document.domain + ':' + '1783');
    $('#publish').click(function(event) {
        var topic = $('#topic').val();
        var message = $('#message').val();
        var qos = $('#qos').val();
        var dataJson = '{"topic": "' + topic + '", "message": "' + message + '", "qos": ' + qos + '}';

        comand="http://localhost:5000/manualMqtt/"+dataJson;
        $.get(comand, function(data, status){
            if(data == "OK") {
                console.log("Mensaje json publicado\n"+dataJson);
            }
            else {
                console.log("Error en publicacion\n"+dataJson);
            }
        });

    });

    $('#subscribe').click(function(event) {
        var topic = $('#subscribe_topic').val();
        var qos = $('#subscribe_qos').val();
        $('#subscribe').hide();
        $('#unsubscribe').show();
        $('#subscribe_topic').prop('readonly', true);
      
        comand="http://localhost:5000/subscribe/"+topic;
        $.get(comand, function(data, status){
            if(data == topic) {
                console.log("Subscrito a:\n"+topic);
            }
            else {
                console.log("Error en Subscribir:\n"+topic);
            }
        });
    });
    $('#unsubscribe').click(function(event) {
        var topic = $('#subscribe_topic').val();
        $('#subscribe').show();
        $('#unsubscribe').hide();
        $('#subscribe_topic').prop('readonly', false);

        comand="http://localhost:5000/unsubscribe/"+topic;
        $.get(comand, function(data, status){
            if(data == topic) {
                console.log("Desubscrito a:\n"+topic);
            }
            else {
                console.log("Error en Desubscribir:\n"+topic);
            }
        });

    });

    var id;

    $('#generar').click(function(event) {
        
        $('#generar').hide();
        $('#detener').show();
        $('#tempSuperior').prop('readonly', true);
        $('#tempInferior').prop('readonly', true);
        $('#frecMensajes').prop('readonly', true);
        
        //Temperatura y Peak to Peak.
        var temp = $('#temperatura').val();
        var pp = $('#peaktopeak').val();

        //Acelerometro d.
        var dX = $('#dX').val();
        var dY = $('#dY').val();
        var dZ = $('#dZ').val();

        //Acelerometro k.
        var kX = $('#kX').val();
        var kY = $('#kY').val();
        var kZ = $('#kZ').val();

        var checkSuperior = document.getElementById("checkSuperior");
        var checkInferior = document.getElementById("checkInferior");
        var checkComm = document.getElementById("checkComm");
        var frecMensajes = Number($('#frecMensajes').val());

        id = setInterval(frame, frecMensajes);

        function frame() {
            console.log("Enviando Mqtt usando comportamiento:");
            //Temperatura Superior e Inferior, junto al Intervalo de frecuencia de envio.
            var tempInferior = Number($('#tempInferior').val());
            var tempSuperior = Number($('#tempSuperior').val());
            
            

            if (checkSuperior.checked == true){                
                tempSuperior+=tempSuperior;
            } 

            if (checkInferior.checked == true){
                tempInferior=-20;
            }

            testNum=Math.floor((Math.random() * (tempSuperior-tempInferior) + tempInferior));
            document.getElementById("statusTemp").innerHTML =testNum;
            tempStatusCheck();
            //console.log("Temp Sup:"+tempSuperior+"Temp Inf:"+tempInferior+"Lim Sup:"+limiteSuperior+"Lim Inf:"+limiteInferior);
            //Data para Mqtt en formato JSON.
            var dataJson = '{"data":{ "kX":'+kX+',"kY":'+kY+',"kZ":'+kZ+',"dX":'+dX+',"dY":'+dY+',"dZ":'+dZ+',"PP":'+pp+',"T":'+testNum+'},"rssi":-86,"freq":903,"seqn":95,"chan":8,"timestamp":"2018-08-01T15:44:39.030889Z"}';
            comand="http://localhost:5000/manualMqtt/"+dataJson;
            $.get(comand, function(data, status){
                if(data == "OK") {
                    console.log("Mensaje json publicado\n"+dataJson);
                }
                else {
                    console.log("Error en publicacion\n"+dataJson);
                }
            });

        }

    });

    $('#detener').click(function(event) {        
        $('#generar').show();
        $('#detener').hide();
        $('#tempSuperior').prop('readonly', false);
        $('#tempInferior').prop('readonly', false);
        $('#frecMensajes').prop('readonly', false);

        clearInterval(id);
    });

    $('#mqttManual').click(function(event) {
        //Temperatura y Peak to Peak.
        var temp = $('#temperatura').val();
        var pp = $('#peaktopeak').val();

        //Acelerometro d.
        var dX = $('#dX').val();
        var dY = $('#dY').val();
        var dZ = $('#dZ').val();

        //Acelerometro k.
        var kX = $('#kX').val();
        var kY = $('#kY').val();
        var kZ = $('#kZ').val();

        //Data para Mqtt en formato JSON.
        var dataJson = '{"data":{ "kX":'+kX+',"kY":'+kY+',"kZ":'+kZ+',"dX":'+dX+',"dY":'+dY+',"dZ":'+dZ+',"PP":'+pp+',"T":'+temp+'},"rssi":-86,"freq":903,"seqn":95,"chan":8,"timestamp":"2018-08-01T15:44:39.030889Z"}';
        comand="http://localhost:5000/manualMqtt/"+dataJson;
        $.get(comand, function(data, status){
            if(data == "OK") {
                console.log("Mensaje json publicado\n"+dataJson);
            }
            else {
                console.log("Error en publicacion\n"+dataJson);
            }
        });

      });

    
    
  });
