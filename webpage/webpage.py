'''
Created on Jul 30, 2018

@author: Andres Oyarce
'''
import json
import sys
from flask import Flask
from flask import render_template
from flask_mqtt import Mqtt
 

app = Flask(__name__)

app.config['SECRET'] = 'my secret key'
app.config['TEMPLATES_AUTO_RELOAD'] = True
app.config['MQTT_BROKER_URL'] = '127.0.0.1'
app.config['MQTT_BROKER_PORT'] = 1783
app.config['MQTT_CLIENT_ID'] = 'Rivet_MQTT_Test_Aoyarce'
app.config['MQTT_USERNAME'] = ''
app.config['MQTT_PASSWORD'] = ''
app.config['MQTT_KEEPALIVE'] = 5
app.config['MQTT_TLS_ENABLED'] = False
#app.config['MQTT_LAST_WILL_TOPIC'] = 'home/lastwill'
#app.config['MQTT_LAST_WILL_MESSAGE'] = 'bye'
#app.config['MQTT_LAST_WILL_QOS'] = 0
app.config['MQTT_REFRESH_TIME'] = 1.0  # refresh time in seconds

# Parameters for SSL enabled
# app.config['MQTT_BROKER_PORT'] = 8883
# app.config['MQTT_TLS_ENABLED'] = True
# app.config['MQTT_TLS_INSECURE'] = True
# app.config['MQTT_TLS_CA_CERTS'] = 'ca.crt'


mqtt = Mqtt(app)
messageD = ""

data_template= json.loads('{"data":{ "kX":-0.08,"kY":0.77,"kZ":-0.75,"dX":1.16,"dY":1.69,"dZ":1.36,"PP":183,"T":20.50},"rssi":-86,"freq":903,"seqn":95,"chan":8,"timestamp":"2018-08-01T15:44:39.030889Z"}')
data_template['rssi']=100
#print(data_template['rssi'], file=sys.stderr)
#print (json.dumps(data_template, sort_keys=True, indent=4, separators=(',', ': ')), file=sys.stderr)

@mqtt.on_connect()
def handle_connect(client, userdata, flags, rc):
    print("test handle mqtt on_connect 2", file=sys.stderr)
    #mqtt.subscribe('test')

@mqtt.on_message()
def handle_mqtt_message(client, userdata, message):
    #print("test handle mqtt on_message", file=sys.stderr)
    print('Received message on topic {}:{}'.format(message.topic, message.payload.decode()), file=sys.stderr)
    global messageD
    messageD=message.payload.decode()
    return 'Mensaje entregado'

@app.route('/check')
def message():
    #print("test handle message", file=sys.stderr)
    global messageD
    aux=messageD
    messageD=""
    print("MSG"+str(aux),file=sys.stderr)
    return aux

@app.route('/unsubscribeAll')
def unsubscribeAll():
    print("unsubscribe All", file=sys.stderr)
    mqtt.unsubscribe_all()
    return "OK"

@app.route('/subscribe/<topic>')
def subscribe(topic):
    print("Subscribiendo a Topico:\n"+str(topic), file=sys.stderr)
    mqtt.subscribe(topic)
    return topic

@app.route('/unsubscribe/<topic>')
def unsubscribe(topic):
    print("DeSubscribiendo a Topico:\n"+str(topic), file=sys.stderr)
    mqtt.unsubscribe(topic)
    return topic

@app.route('/manualMqtt/<data>')
def manualMqtt(data):
    print("Manual Mqtt message:\n"+str(data), file=sys.stderr)
    mqtt.publish("test", data)
    return "OK"

@app.route('/')
def index():
    #return 'Index Page'
    return render_template('qos_template.html')

@app.route('/temp/<tempValue>')
def setTemp(tempValue):
    pass


@app.route('/user/<username>')
def show_user_profile(username):
    # show the user profile for that user
    mqtt.publish("test", username)
    return 'User %s' % username


@app.route('/post/<int:post_id>')
def show_post(post_id):
    # show the post with the given id, the id is an integer
    return 'Post %d' % post_id

@app.route('/path/<path:subpath>')
def show_subpath(subpath):
    # show the subpath after /path/
    return 'Subpath %s' % subpath

@app.route('/hello/')
@app.route('/hello/<name>')
def hello(name=None):
    return render_template('hello.html', my_string="Wheeeee!", my_list=[0,1,2,3,4,5])
    #return render_template('hello.html', name=name)
    
@app.route("/home")
def home():
    return render_template(
        'hello.html', my_string="Wheeeee!", 
        my_list=[0,1,2,3,4,5], title="Home")

@app.route("/about")
def about():
    return render_template(
        'hello.html', my_string="Wheeeee!", 
        my_list=[0,1,2,3,4,6], title="About")

@app.route("/contact")
def contact():
    return render_template(
        'hello.html', my_string="Wheeeee!", 
        my_list=[0,1,2,3,4,5], title="Contact Us")
    
@app.route('/init')
def initWeb(name=None):
    return render_template('index.html', my_string="Wheeeee!", my_list=[0,1,2,3,4,5])
